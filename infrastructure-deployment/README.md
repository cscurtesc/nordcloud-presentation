# 3-tier-node Application Infrastructure Deployment

This git was build to meet the customer expectations expressed in the e-mail received from Namik P. on  Feb 2, 2021, 9:57 AM (forwarded below)

The requirements for the test project are:
You want to design a continuous delivery architecture for a scalable and secure 3 tier Node application. Application to use can be found on https://git.toptal.com/namikp/node-3tier-app2

- Both web and API tiers should be exposed to the internet and DB tier should not be accessible from the internet.
- You should fork the repository and use it as the base for your system.
- You need to create resources for all the tiers.
- The architecture should be completely provisioned via some infrastructure as a code tool.
- Presented solution must handle server (instance) failures.
- Components must be updated without downtime in service.
- The deployment of new code should be completely automated (bonus points if you create tests and include them into the pipeline).
- The database and any mutable storage need to be backed up at least daily.
- All relevant logs for all tiers need to be easily accessible (having them on the hosts is not an option).
- Implement monitoring and logging using self-provisioned tools. Usage of managed services for monitoring/logging is not permitted.
- You should fork the repository and use it as the base for your system.
- You should be able to deploy it on one larger Cloud provider: AWS / Google Cloud / Azure / DigitalOcean / RackSpace.
- The system should present relevant historical metrics to spot and debug bottlenecks.


As a solution, please commit to the git repo the following:
- An architectural diagram / PPT to explain your architecture during the interview.
- All the relevant configuration scripts (Terraform/Chef/Puppet/cfengine/ansible/cloud formation)
- All the relevant runtime handling scripts (start/stop/scale nodes).
- All the relevant backup scripts.




## Prerequisites

I am assuming that the customer:
- is using a modern version of Linux or MacOS and he/she has got the latest versions of Ansible and git installed. The below procedure has been tested by performing the deployment from a AWS Cloud9 Terminal
- has got root access to AWS, and he/she is able to create the "toptal-project" IAM account having admin rights, and having valid CLI access keys enabled
- owns a hub.docker.com and a gitlab.com repository

Installing boto3

Ansible depends on the Python module boto3 to communiate with AWS API. So, boto3 needs to be installed on your machine. Issue the following command on your terminal:
```
pip3 install ansible
pip3 install boto3
```

Clone the git repo:
```
git clone https://gitlab.com/cscurtesc/nordcloud-presentation/; cd nordcloud-presentation
```
Store your credentials in Ansible vault.


```
[root@world my-project]# ansible-vault create infrastructure-deployment/group_vars/aws_keys.yml
Vault password:
Once opened, add the following to it:

aws_access_key: XXXXXXXXX
aws_secret_key: XXXXXX
gitlab_token: XXXX
dockerhub_token: XXXXX
dockerhub_userid: XXXXX
dockerhub_email: XXXXX
postgres_pass: XXXX
```
_*) where "aws_access_key" and "aws_secret_key" are the credentials of your AWS IAM USER with admin role attached; "gitlab_token" and "dockerhub_token" are the tokens of your gitlab and Docker hub repos; dockerhub_userid and dockerhub_email are the details of your Dockerhub account and  the  postgres_pass can be any password you choose of min. 8 characters - it will be used on the postgres RDS initialization_

## Installing

We now have everything in place and we're ready to create the infrastructure.
```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook infrastructure-deployment/provision-ec2.yml --ask-vault-pass
Vault password:
```

The Ansible scripts will:

- Deploys the 3-tier-node app, in high available setup by leveraging the power of Kubernetes(AWS EKS) 
- Create the Network setup follwing the AWS  Well-Arhitected best practices
- Launch an EC2 instance that will be used by the GitLab Runners. This instance will also be used as a Bastion host to control the K8s cluster
- Deploy several tools and create the environment to be able to control the Kubernetes cluster, from the Bastion host
- Deploy Docker and setup the Docker Hub connection, needed to push images to the Registry
- Deploy the Logging and Monitoring infrastructure(Loki, Prometheus, Grafana)

### Post-Installation Tests 


- check the Ansible output, make sure no errors are present
- the installation script should mention "INSTALLATION COMPLETED" and provide the IP address of the EC2 instance which will be used to interact with the Kube cluster
- ssh on the EC2 instance and run the below commands. The application pods should be running and an external IP should be assigned to the API and WEB services

```
[root@ip-172-31-18-218 infrastructure-deployment]# kubectl get pods
NAME                                 READY   STATUS      RESTARTS   AGE
toptal-api-nodejs-5756d6954d-dj2wm   1/1     Running     0          108m
toptal-api-nodejs-5756d6954d-gnjt2   1/1     Running     0          108m
toptal-web-nodejs-66676b5ff8-rd8pn   1/1     Running     0          147m
toptal-web-nodejs-577c7fc66f-8nwrk   1/1     Running     0          50m
[root@ip-172-31-18-218 infrastructure-deployment]# kubectl get services
NAME                TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)          AGE
kubernetes          ClusterIP      10.100.0.1       <none>                                                                    443/TCP          3h10m
toptal-api-nodejs   LoadBalancer   10.100.82.51     ae33b9537a7084214b178d11b0a6c52d-458864519.us-east-2.elb.amazonaws.com    80:31751/TCP     3h3m
toptal-web-nodejs   LoadBalancer   10.100.127.232   a6c33b15c90fc4bbba42067f6b5eba66-1381326063.us-east-2.elb.amazonaws.com   80:32406/TCP     3h3m
```
- curl or navigate with the browser to the 2 external IPs, make sure you get an answer



## Monitoring and Logging

Monitoring and logging has been implemented using the following self-provisioned tools: Prometheus, Loki and Grafana

### Prometheus

I used Prometheus as matrix-collector and a Blackbox Exporter to define some custom checks (see Sources below). 
Prometheus UI is not reachable from Internet but can be access by configuring a port forwarding as below:
```
#Get the Prometheus server URL by running these commands in the same shell:
export POD_NAME=$(kubectl get pods --namespace prometheus -l \"app=prometheus,component=server\" -o jsonpath=\"{.items[0].metadata.name}\")",
kubectl --namespace prometheus port-forward $POD_NAME 9090"
```  
### Loki

Loki is a log aggregation system inspired by Prometheus. Same as Prometheus, it will deploy a pod(agent) on each worker nods to scrap the application logs. I prefered it over other similar tools because is lightweight and integrates well with Grafana. Same as Prometheus, Loki won't be reachable from Internet, but you can access it from your PC using a port-forwarding (see above)

### Grafana

Used to display graphs and logs collected by Loki and Prometheus.
Grafana's UI is reachable from Internet, the service waspublished through a LoadBalancer.  The login details are displayed into the Ansible output.

There is a minimum setup needed to be performed:

ADD Loki as data source
login to the UI and Add Data Source -> Loki and under url paste this: http://loki-stack.loki-stack:3100 and SAVE. After this application logs can be viewed under EXPLORE(left hand side menu) ->Loki as data source -> Log Label -> app

ADD Dashboards
click on the + sign(left hand side menu)  and select IMPORT. On the new page under the ADD from grafana.com paste the following IDs: 3119, 13041, 6417 (one by one and selecting Prometheus as data source)



## CI/CD

For this task I used the CI/CD feature provided by GitLab.
The setup was as follows:
- I deployed and registered a Runner using the information/code found here: Setting -> CI/CD -> Runners ->Show Runners Installation Instructions

NOTE: two small changes were done:

  on the installation page, I have deployed the runner to run as priviledged user

  on the runner's registration step I used the following commad:

  ```
  gitlab-runner register --url https://gitlab.com/ --registration-token "TOKEN_FROM_POPUP_SCREEN" \
  --description "NAME_OF_EC2_INSTANCE" --tag-list toptal --executor shell
  ```

- The deployment of the runner was done on the EC2 Instance used for the Kubernetes cluster deployment
- The file containing the runner instructions is called: .gitlab-ci.yml and was uploaded on GitLab.
- I've created a Docker image for each application tier and uploaded them to a private Docker Hub repository. To interact with the DockerHub repo you need to instruct the runner to provide login credentials. The credentials are stored into $DOCKER_HUB_USER and $DOCKER_HUB_PASS variables that were defined using the UI: Setting -> CI/CD -> Variables

I tried to keep it as simple as possible: for each git commit I rebuild the API and/or WEB Docker images and I push them to the Docker Registry. Once a new image is created the runner will "kubectl apply" the app/web definition files fetching the new images from the Registry

To test this integration, you should fork my gitlab code to your own gitlab and setup the Runner as explained above.

## Scalling up

The EKS worker nodes are part of a auto-scaling group, meaning that if traffic hits the application, new pods and possible new worker nodes will be added to the cluster
The application follows the HA setup but if new instances need to be manually launched, this can be achieve by using the following commands:


```
kubectl scale --replicas=3 deployment toptal-api-nodejs
kubectl scale --replicas=3 deployment toptal-web-nodejs
```



## Built With

* [AWS](https://aws.amazon.com/ ) - Cloud provider
* [EKS](https://aws.amazon.com/eks/) - Kubernetes on cloud
* [Ansible](https://www.ansible.com/) - Deployment and config management
* [Loki](https://grafana.com/docs/grafana/latest/datasources/loki/) - Logging 
* [Grafana](https://grafana.com/) - Dashboards and vizualization
* [Prometheus](https://prometheus.io/) - Monitoring and matrix-collector
* [Docker](https://www.docker.com/) - Container solution


## Sources

- https://www.robustperception.io/checking-for-http-200s-with-the-blackbox-exporter
- https://docs.gitlab.com/ee/ci/runners/README.html
- https://www.scaleway.com/en/docs/use-loki-to-manage-k8s-application-logs/
- https://staffordwilliams.com/blog/2020/05/06/postgresql-in-kubernetes/


## Application diagram

![App Diagram](./toptal-project.png "App diagram")

## Remove 

eksctl delete cluster "toptal-eksctl"  --disable-nodegroup-eviction

## Scale down
eksctl get cluster
eksctl get nodegroup --cluster toptal-eksctl
eksctl scale nodegroup --cluster toptal-eksctl --name nodegroup --nodes 1








