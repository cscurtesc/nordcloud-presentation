# Sample 3tier app
This repo contains code for a Node.js multi-tier application and the automation scripts to deploy it on a AWS Kubernetes custer

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.

The deployment procedure and the application diagram can be found inside the "infrastructure-deployment"  folder
